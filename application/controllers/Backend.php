<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// we will use the composer autoloader to make sure we autoload the packages installed by composer
require_once APPPATH.'../vendor/autoload.php';

class Backend extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('securityuser');
        $this->load->library('grocery_CRUD');

        date_default_timezone_set("America/Guayaquil");
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -  
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function index() {
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Inicio";

            $dataHeader['titlePage'] = $titulo;
            $dataContent['debug'] = $debug;

            $data['header'] = $this->load->view('backend/header', $dataHeader);
            $data['menu'] = $this->load->view('backend/menu-admin', $dataHeader );

            $data['content'] = $this->load->view('backend/index', array());
            $data['footer'] = $this->load->view('backend/footer', array());
        } else {
            redirect("backend/login");
        }
    }

    public function login() {
        $debug = false;

        $dataHeader['titlePage'] = 'Inicio Admin';
        $dataContent['debug'] = $debug;

        $data['header'] = $this->load->view('backend/header', $dataHeader);
        $data['content'] = $this->load->view('backend/login', $dataContent);
        $data['footer'] = $this->load->view('backend/footer', array());
    }

     public function logout() {

        $securityUser = new SecurityUser();
        $securityUser->logout();
        $debug = false;

        $dataHeader['titlePage'] = 'Inicio Admin';
        $dataContent['debug'] = $debug;

        $data['header'] = $this->load->view('backend/header', $dataHeader);
        $data['content'] = $this->load->view('backend/login', $dataContent);
        $data['footer'] = $this->load->view('backend/footer', array());
    }

    public function autentificar() {

        $username = $this->input->post("username");
        $password = $this->input->post("password");

        $securityUser = new SecurityUser();

        $securityUser->login($username, $password);

        if ( $this->session->userdata('nombre') != "") {
            redirect("backend/index");
        }else{
            redirect("backend/logout");
        }

    }
    
    function securityCheck() {
        $usuario = $this->session->userdata('username');
        if ($usuario == "") {
            return false;
        } else {
            return true;
        }
    }

    function _add_default_date_value(){
        $value = !empty($value) ? $value : date("d/m/Y");
        $return = '<input type="text" name="date" value="'.$value.'" class="datepicker-input" /> ';
        $return .= '<a class="datepicker-input-clear" tabindex="-1">Clear</a> (dd/mm/yyyy)';
        return $return;
    }

    function encrypt_pw($post_array, $pk) {
        if(!empty($post_array['password'])) {
            $data['password'] = md5($post_array['password']);
            $this->db->where('id', $pk);
            $this->db->update('usuario', $data);
        }
    }

    //////////////////////////////////////////////////////////////
    // ALTAVOZ CajaNegraEC - Tratamiento data
    //////////////////////////////////////////////////////////////

    //Function to generate the map field

    
    //////////////////////////////////////////////////////////////
    // ALTAVOZ CajaNegraEC - Redimensionar Imagenes
    //////////////////////////////////////////////////////////////



    //////////////////////////////////////////////////////////////
    // QHH CajaNegraEC - Funciones para mantenimientos
    //////////////////////////////////////////////////////////////




    //////////////////////////////////////////////////////////////
    // ALTAVOZ CajaNegraEC - Mantenimientos
    //////////////////////////////////////////////////////////////
    
    public function usuario(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Usuario";

            $crud = new grocery_CRUD();
            $crud->set_table("usuario");
            $crud->set_subject( $titulo );
            $crud->display_as('username','Username');
            $crud->display_as('password','Contraseña');
            $crud->display_as('nombre','Nombre');
            $crud->display_as('apellido','Apellido');
            $crud->display_as('email','Email');
            $crud->display_as('telefono','Telefono');
            $crud->display_as('estado','Estado');

            $crud->field_type('estado', 'dropdown', array(
                '1' => 'Activo',
                '0' => 'Inactivo'
            ));

            $crud->field_type('password', 'password');
            $crud->callback_after_insert(array($this,'encrypt_pw'));
            $crud->callback_after_update(array($this,'encrypt_pw'));
            
            $crud->columns('username', 'password', 'nombre','apellido', 'email', 'telefono', 'estado' );
            $crud->fields('username', 'password', 'nombre','apellido', 'email', 'telefono', 'estado');
            $crud->required_fields('username', 'password', 'nombre','apellido', 'email', 'telefono', 'estado');

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();
            
            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;

            $dataContent['output'] = $output->output;

            $data['header'] = $this->load->view('backend/header', $dataHeader);
            $data['menu'] = $this->load->view('backend/menu-admin', $dataHeader );

            $data['content'] = $this->load->view('backend/admin-content', $dataContent);
            $data['footer'] = $this->load->view('backend/footer', $dataFooter);
        } else {
            redirect("backend/login");
        }
    }

    public function pais(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "País";

            $crud = new grocery_CRUD();
            $crud->set_table("pais");
            $crud->set_subject( $titulo );
            $crud->display_as('Nombre','Nombre de categoría');
            $crud->display_as('Estado','Estado');

            $crud->field_type('Estado', 'dropdown', array(
                '1' => 'Activo',
                '0' => 'Inactivo'
            ));


            $crud->columns('Nombre','Estado' );
            $crud->fields('Nombre','Estado');
            $crud->required_fields('Nombre','Estado');

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;

            $dataContent['output'] = $output->output;

            $data['header'] = $this->load->view('backend/header', $dataHeader);
            $data['menu'] = $this->load->view('backend/menu-admin', $dataHeader );

            $data['content'] = $this->load->view('backend/admin-content', $dataContent);
            $data['footer'] = $this->load->view('backend/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }

    public function ciudad(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Ciudad";

            $crud = new grocery_CRUD();
            $crud->set_table("ciudad");
            $crud->set_subject( $titulo );
            $crud->display_as('Nombre','Nombre de Ciudad');
            $crud->display_as('Estado','Estado');
            $crud->display_as('IdProvincia','Provincia');

            $crud->field_type('Estado', 'dropdown', array(
                '1' => 'Activo',
                '0' => 'Inactivo'
            ));

            $crud->set_relation('IdProvincia','provincia','Nombre');

            $crud->columns('Nombre','Estado', 'IdProvincia' );
            $crud->fields('Nombre','Estado', 'IdProvincia');
            $crud->required_fields('Nombre','Estado', 'IdProvincia');

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;

            $dataContent['output'] = $output->output;

            $data['header'] = $this->load->view('backend/header', $dataHeader);
            $data['menu'] = $this->load->view('backend/menu-admin', $dataHeader );

            $data['content'] = $this->load->view('backend/admin-content', $dataContent);
            $data['footer'] = $this->load->view('backend/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }
    
    public function provincia(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Provincia";

            $crud = new grocery_CRUD();
            $crud->set_table("provincia");
            $crud->set_subject( $titulo );
            $crud->display_as('Nombre','Nombre de Provincia');
            $crud->display_as('Estado','Estado');
            $crud->display_as('IdPais','País');

            $crud->field_type('Estado', 'dropdown', array(
                '1' => 'Activo',
                '0' => 'Inactivo'
            ));

            $crud->set_relation('IdPais','pais','Nombre');

            $crud->columns('Nombre','Estado', 'IdPais' );
            $crud->fields('Nombre','Estado', 'IdPais');
            $crud->required_fields('Nombre','Estado', 'IdPais');

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;

            $dataContent['output'] = $output->output;

            $data['header'] = $this->load->view('backend/header', $dataHeader);
            $data['menu'] = $this->load->view('backend/menu-admin', $dataHeader );

            $data['content'] = $this->load->view('backend/admin-content', $dataContent);
            $data['footer'] = $this->load->view('backend/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }

    public function categoria(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Categorías";

            $crud = new grocery_CRUD();
            $crud->set_table("categoria");
            $crud->set_subject( $titulo );
            $crud->display_as('Nombre','Nombre de Categoría');
            $crud->display_as('Estado','Estado');

            $crud->field_type('Estado', 'dropdown', array(
                '1' => 'Activo',
                '0' => 'Inactivo'
            ));

            $crud->columns('Nombre','Estado' );
            $crud->fields('Nombre','Estado');
            $crud->required_fields('Nombre','Estado');

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;

            $dataContent['output'] = $output->output;

            $data['header'] = $this->load->view('backend/header', $dataHeader);
            $data['menu'] = $this->load->view('backend/menu-admin', $dataHeader );

            $data['content'] = $this->load->view('backend/admin-content', $dataContent);
            $data['footer'] = $this->load->view('backend/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }

    public function local(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Local";

            $crud = new grocery_CRUD();
            $crud->set_table("local");
            $crud->set_subject( $titulo );
            $crud->display_as('Nombre','Nombre de Local');
            $crud->display_as('Direccion','Dirección');
            $crud->display_as('Coordenadas','Coordenadas');
            $crud->display_as('Telefono1','Telefono1');
            $crud->display_as('Telefono2','Telefono2');
            $crud->display_as('Telefono3','Telefono3');
            $crud->display_as('Telefono4','Telefono4');
            $crud->display_as('Estado','Estado');
            $crud->display_as('IdCiudad','Ciudad');

            $crud->field_type('Estado', 'dropdown', array(
                '1' => 'Activo',
                '0' => 'Inactivo'
            ));

            $crud->set_relation('IdCiudad','ciudad','Nombre');

            $crud->columns('Nombre', 'Direccion', 'Coordenadas', 'Telefono1', 'Telefono2', 'Telefono3', 'Telefono4','Estado', 'IdCiudad' );
            $crud->fields('Nombre','Estado', 'Direccion', 'Coordenadas', 'Telefono1', 'Telefono2', 'Telefono3', 'Telefono4', 'IdCiudad');
            $crud->required_fields('Nombre', 'Direccion', 'Coordenadas', 'Telefono1', 'Telefono2', 'Telefono3', 'Telefono4','Estado', 'IdCiudad');

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;

            $dataContent['output'] = $output->output;

            $data['header'] = $this->load->view('backend/header', $dataHeader);
            $data['menu'] = $this->load->view('backend/menu-admin', $dataHeader );

            $data['content'] = $this->load->view('backend/admin-content', $dataContent);
            $data['footer'] = $this->load->view('backend/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }

    public function suscriptor(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Suscriptor";

            $crud = new grocery_CRUD();
            $crud->set_table("suscriptor");
            $crud->set_subject( $titulo );
            $crud->display_as('Email','Email');
            $crud->display_as('Estado','Estado');
            $crud->display_as('IdLocal','Local');

            $crud->field_type('Estado', 'dropdown', array(
                '1' => 'Activo',
                '0' => 'Inactivo'
            ));

            $crud->set_relation('IdLocal','local','Nombre');

            $crud->columns('Nombre','Estado', 'IdLocal' );
            $crud->fields('Nombre','Estado', 'IdLocal' );
            $crud->required_fields('Nombre','Estado', 'IdLocal' );

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;

            $dataContent['output'] = $output->output;

            $data['header'] = $this->load->view('backend/header', $dataHeader);
            $data['menu'] = $this->load->view('backend/menu-admin', $dataHeader );

            $data['content'] = $this->load->view('backend/admin-content', $dataContent);
            $data['footer'] = $this->load->view('backend/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }

    public function redsocial(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Red Social";

            $crud = new grocery_CRUD();
            $crud->set_table("redsocial");
            $crud->set_subject( $titulo );
            $crud->display_as('Nombre','Red Social');
            $crud->display_as('Logo','Logo');
            $crud->display_as('Estado','Estado');

            $crud->field_type('Estado', 'dropdown', array(
                '1' => 'Activo',
                '0' => 'Inactivo'
            ));

            $crud->set_field_upload('Logo', 'assets/altavoz/logo');

            $crud->columns('Nombre', 'Logo', 'Estado' );
            $crud->fields('Nombre', 'Logo', 'Estado' );
            $crud->required_fields('Nombre', 'Logo', 'Estado' );

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;

            $dataContent['output'] = $output->output;

            $data['header'] = $this->load->view('backend/header', $dataHeader);
            $data['menu'] = $this->load->view('backend/menu-admin', $dataHeader );

            $data['content'] = $this->load->view('backend/admin-content', $dataContent);
            $data['footer'] = $this->load->view('backend/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }

    public function promocion(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Promoción";

            $crud = new grocery_CRUD();
            $crud->set_table("promocion");
            $crud->set_subject( $titulo );
            $crud->display_as('Nombre','Nombre de Promoción');
            $crud->display_as('Descripcion','Descripción');
            $crud->display_as('UrlImagen','Imagen');
            $crud->display_as('FechaInicio','Fecha de Inicio');
            $crud->display_as('FechaFin','Fecha Final');
            $crud->display_as('Estado','Estado');
            $crud->display_as('IdLocal','Local');

            $crud->field_type('Estado', 'dropdown', array(
                '1' => 'Activo',
                '0' => 'Inactivo'
            ));

            $crud->set_relation('IdLocal','local','Nombre');

            $crud->set_field_upload('Logo', 'assets/altavoz/logo');

            $crud->columns('Nombre', 'Descripcion', 'UrlImagen', 'FechaInicio', 'FechaFin', 'Estado', 'IdLocal' );
            $crud->fields('Nombre', 'Descripcion', 'UrlImagen', 'FechaInicio', 'FechaFin', 'Estado', 'IdLocal' );
            $crud->required_fields('Nombre', 'Descripcion', 'UrlImagen', 'FechaInicio', 'FechaFin', 'Estado', 'IdLocal' );

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;

            $dataContent['output'] = $output->output;

            $data['header'] = $this->load->view('backend/header', $dataHeader);
            $data['menu'] = $this->load->view('backend/menu-admin', $dataHeader );

            $data['content'] = $this->load->view('backend/admin-content', $dataContent);
            $data['footer'] = $this->load->view('backend/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }
    
    public function galerialocal(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Galeria Local";

            $crud = new grocery_CRUD();
            $crud->set_table("galerialocal");
            $crud->set_subject( $titulo );
            $crud->display_as('UrlImagen','Imagen');
            $crud->display_as('Estado','Estado');
            $crud->display_as('IdLocal','Local');

            $crud->field_type('Estado', 'dropdown', array(
                '1' => 'Activo',
                '0' => 'Inactivo'
            ));

            $crud->set_relation('IdLocal','local','Nombre');

            $crud->set_field_upload('Logo', 'assets/altavoz/logo');

            $crud->columns('UrlImagen', 'Estado', 'IdLocal' );
            $crud->fields('UrlImagen', 'Estado', 'IdLocal' );
            $crud->required_fields('UrlImagen', 'Estado', 'IdLocal' );

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;

            $dataContent['output'] = $output->output;

            $data['header'] = $this->load->view('backend/header', $dataHeader);
            $data['menu'] = $this->load->view('backend/menu-admin', $dataHeader );

            $data['content'] = $this->load->view('backend/admin-content', $dataContent);
            $data['footer'] = $this->load->view('backend/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }

    public function local_redsocial(){
        $debug = false;

        if ($this->securityCheck()) {
            $titulo = "Local Red Social";

            $crud = new grocery_CRUD();
            $crud->set_table("local_redsocial");
            $crud->set_subject( $titulo );
            $crud->display_as('IdLocal','Local');
            $crud->display_as('IdRedSocial','Red Social');
            $crud->display_as('Url','Link');
            $crud->display_as('Estado','Estado');
            $crud->display_as('local_redsocialcol','Local Red Social');

            $crud->field_type('Estado', 'dropdown', array(
                '1' => 'Activo',
                '0' => 'Inactivo'
            ));

            $crud->set_relation('IdLocal','local','Nombre');
            $crud->set_relation('IdRedSocial','redsocial','Nombre');

            $crud->set_field_upload('Logo', 'assets/altavoz/logo');

            $crud->columns('IdLocal', 'IdRedSocial', 'Url', 'Estado', 'local_redsocialcol' );
            $crud->fields('IdLocal', 'IdRedSocial', 'Url', 'Estado', 'local_redsocialcol' );
            $crud->required_fields('IdLocal', 'IdRedSocial', 'Url', 'Estado', 'local_redsocialcol' );

            $crud->unset_read();
            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();

            $dataHeader['titlePage'] = $titulo;
            $dataHeader['css_files'] = $output->css_files;
            $dataFooter['js_files'] = $output->js_files;
            $dataContent['debug'] = $debug;

            $dataContent['output'] = $output->output;

            $data['header'] = $this->load->view('backend/header', $dataHeader);
            $data['menu'] = $this->load->view('backend/menu-admin', $dataHeader );

            $data['content'] = $this->load->view('backend/admin-content', $dataContent);
            $data['footer'] = $this->load->view('backend/footer', $dataFooter);
        } else {
            redirect("admin/login");
        }
    }

}