<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    /**
     * Description of Security
     *
     * @author Hector
     */
    class SecurityUser extends CI_Model {

        var $usuario = "";
        var $password = "";
        var $password_anterior = "";
        var $nombre = "";
        var $correo = "";
        var $fecha_creacion = "";
        var $fecha_modificacion = "";
        var $estado = "";


        function __construct() {
            parent::__construct();
            $this->load->database();
            $this->load->library('session');
        }
        

        function login($username , $password ){
          $usuario = $this->db->get_where("usuario", array('username'=> $username , 'password' => md5($password) ))->row();  
          
          if($usuario){
            $data = array("username" => $usuario->username, "nombre" => $usuario->nombre, "apellido" => $usuario->apellido,);
            $this->session->set_userdata( $data );
            return true;
          }
        }
        

        function logout(){        
          $this->session->sess_destroy();        
        }

    }
?>