        <div class="col-lg-10 col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <div class="box-inner goapp-box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2>
                                <i class="glyphicon glyphicon-folder-open"></i>&nbsp;&nbsp;<?php echo $titlePage; ?>
                            </h2>
                        </div>
                        <div class="box-content text-center">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-sm-4 col-sm-offset-4 text-center">
                                        <img src="<?php echo base_url('public/img/logo.png'); ?>" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <h1>
                                Bienvenido <strong><?php echo $this->session->user_login; ?></strong>
                            </h1>
                        </div>
                    </div>
                </div>
            </div><!--/row-->
        </div>
    </div>
</div> <!-- inicio ch-container -->