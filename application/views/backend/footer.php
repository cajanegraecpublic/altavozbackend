    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" fclass="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <hr>
        <footer class="row">
            <p class="col-md-9 col-sm-9 col-xs-12 copyright">
                &copy; <a href="#" target="_blank">ALTAVOZ</a> <?php echo date('Y') ?>
            </p>

            <p class="col-md-3 col-sm-3 col-xs-12 powered-by">
                Desarrollado por: <a href="#">ALTAVOZ</a>
            </p>
        </footer>
    </div><!--/.fluid-container-->


    <?php if (isset($js_files)): ?>

        
        <!-- grocerycrud -->
        <?php foreach($js_files as $file): ?>
            <script type="text/javascript" src="<?php echo $file; ?>"></script>
        <?php endforeach; ?>
        <!-- grocerycrud -->


        <?php if (isset($js_files_dd)): ?>
            <!-- Dependent Dropdown -->
            <?php echo $js_files_dd; ?>
            <!-- Dependent Dropdown -->            
        <?php endif ?>
    <?php else: ?>
        <!-- jQuery -->
        

        <?php /*
        <script type="text/javascript" src="<?php echo base_url('public/bower_components/jquery/jquery.min.js'); ?>"></script>
        */ ?>
    <?php endif ?>
    
    
    <!-- Bootstrap -->
    

    <!-- Moment -->
    
    

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- library for cookie management -->
    

    <!-- calender plugin -->
    
    <!-- data table plugin -->
    
    <!-- select or dropdown enhancer -->
   
    
    <!-- plugin for gallery image view -->
    
    
    <!-- notification plugin -->
    

    <!-- library for making tables responsive -->
    
    
    <!-- tour plugin -->
    

    <!-- star rating plugin -->
    

    <!-- for iOS style toggle switch -->
    
    
    <!-- autogrowing textarea plugin -->
    
    <!-- multiple file upload plugin -->
   
    <!-- history.js for cross-browser state change on ajax -->
    

    <!-- application script for Charisma demo -->
    

    <!-- application script for placehorder in crud -->
    
    
    </body>
</html>