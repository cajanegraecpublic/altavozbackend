<div class="container">
    <div class="row">
        <div class="col-md-12 center login-header">
            <h2>Bienvenido a </h2>
        </div>
    </div><!--/row-->

    <div class="row">
        <div class="well goapp-box-inner col-md-5 center login-box borde-recto">
            <div class="col-sm-4 col-sm-offset-4 center">
                <img src="<?php echo base_url('public/img/logo.png'); ?>" title="" alt="" class="img-responsive obj-centrar" />
            </div>
            <br />
            <div class="alert alert-info borde-recto">
                Por favor ingrese su usuario y contraseña
            </div>
            <?php echo form_open('backend/autentificar' , array('class' => 'form-horizontal', 'id' => 'frm-login')); ?>  
                <fieldset>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <?php echo form_input(array(
                            'name' => 'username',
                            'value' => '',
                            'placeholder' => 'username',
                            'class' => 'form-control input-lg input-altavoz',
                        ));?>
                    </div>
                    <div class="clearfix"></div><br>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <?php echo form_password(array(
                            'name' => 'password',
                            'value' => '',
                            'placeholder' => 'contraseña',
                            'class' => 'form-control input-lg input-altavoz',
                        ));?>
                    </div>
                    <div class="clearfix"></div>

                    <?php /*
                    <div class="input-prepend">
                        <label class="remember" for="remember"><input type="checkbox" id="remember"> Recordarme</label>
                    </div>
                    <div class="clearfix"></div>
                    */ ?>

                    <p class="center col-md-6">
                        <button type="submit" class="btn btn-altavoz btn-lg">Iniciar sesión</button>
                    </p>
                </fieldset>
            <?php echo form_close(); ?>
        </div>
    </div><!--/row-->
</div>