<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Altavoz :: <?php echo $titlePage; ?> </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">


        <!-- The styles -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/bootstrap.min.css'); ?>">

        <!-- Fuentes -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/fonts/fuentes.css'); ?>">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Dosis:300,400,500">
        
        <?php if (isset($css_files)): ?>
            <!-- grocerycrud -->
            <?php foreach($css_files as $file): ?>
                <link rel="stylesheet" type="text/css" href="<?php echo $file; ?>" />
            <?php endforeach; ?>
            <!-- grocerycrud -->
        <?php endif ?>

        <!-- Template -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/charisma-app.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/custom-bootstrap-margin-padding.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/estilo.css'); ?>">

        <!-- The fav icon -->






        <script type="text/javascript">
            var base_url = '<?php echo base_url(); ?>';

            var js_site_url = function( urlText ){
                var urlTmp = "<?php echo site_url('" + urlText + "'); ?>";
                return urlTmp;
            }

            var js_base_url = function( urlText ){
                var urlTmp = "<?php echo base_url('" + urlText + "'); ?>";
                return urlTmp;
            }
        </script>

        <script src='https://www.google.com/recaptcha/api.js'></script>

    </head>

<body>